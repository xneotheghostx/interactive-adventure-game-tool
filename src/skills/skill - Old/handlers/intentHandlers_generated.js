var processUtterance = require('./processUtterance')

module.exports = {
	"ResetStateIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "starten" )
	},
	"RestoreStateIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "fortsetzen" )
	},
	"RepeatOptionsIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "auswahl wiederholen" )
	},
	"RepeatSceneIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nochmal" )
	},
	"GoBackIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zurück" )
	},
	"NextIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "überspringen" )
	},
	"TippIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "tipp" )
	},
	"InventarIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "inventar" )
	},
	"AMAZON.HelpIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "hilfe" )
	},
	"AMAZON.StopIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "stop" )
	},
	"AMAZON.CancelIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "abbrechen" )
	},
	"StartIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "start" )
	},
	"NachLinksIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nach links" )
	},
	"SchnellWeiterIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "schnell weiter" )
	},
	"WeiterIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "weiter" )
	},
	"ZumStrandhausIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum strandhaus" )
	},
	"BetretenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "betreten" )
	},
	"ZumLeuchtturmIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum leuchtturm" )
	},
	"RausHierIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "raus hier" )
	},
	"WeiterLesenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "weiter lesen" )
	},
	"FackelNehmenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "fackel nehmen" )
	},
	"MirEgalIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "mir egal" )
	},
	"SchrankDurchsuchenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "schrank durchsuchen" )
	},
	"SchubladeOeffnenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "schublade oeffnen" )
	},
	"StrandhausDurchsuchenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "strandhaus durchsuchen" )
	},
	"OeffneBodenluckeIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "oeffne bodenlucke" )
	},
	"StrandhausVerlassenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "strandhaus verlassen" )
	},
	"LinksZurHoehleIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "links zur hoehle" )
	},
	"ZumStrandIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum strand" )
	},
	"GeradeausIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "geradeaus" )
	},
	"HoehleOeffnenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "hoehle oeffnen" )
	},
	"SeiteneingangIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "seiteneingang" )
	},
	"ZumLagerIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum lager" )
	},
	"HoehleBetretenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "hoehle betreten" )
	},
	"JaIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "ja" )
	},
	"LeicheBegrabenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "leiche begraben" )
	},
	"WeiterReinIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "weiter rein" )
	},
	"LinksIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "links" )
	},
	"WeiterRunterIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "weiter runter" )
	},
	"RechtsIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "rechts" )
	},
	"MuenzeEinsetzenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "muenze einsetzen" )
	},
	"NochWeiterRunterIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "noch weiter runter" )
	},
	"FackelAnzuendenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "fackel anzuenden" )
	},
	"RunterGehenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "runter gehen" )
	},
	"WeiterGehenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "weiter gehen" )
	},
	"TuerOeffnenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "tuer oeffnen" )
	},
	"KarteBenutzenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "karte benutzen" )
	},
	"LinkeAbzweigungIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "linke abzweigung" )
	},
	"NachObenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nach oben" )
	},
	"RechteAbzweigungIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "rechte abzweigung" )
	},
	"ZumWasserfallIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum wasserfall" )
	},
	"InDenVorraumIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "in den vorraum" )
	},
	"DurchDenWasserfallIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "durch den wasserfall" )
	},
	"AusDenDschungelIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "aus den dschungel" )
	},
	"ZumFlugzeugIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum flugzeug" )
	},
	"BenzinHolenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "benzin holen" )
	},
	"EinsteigenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "einsteigen" )
	},
	"ZurHolzhuetteIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zur holzhuette" )
	},
	"HolztuerOeffnenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "holztuer oeffnen" )
	},
	"HuetteVerlassenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "huette verlassen" )
	},
	"PilotenZuhoerenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "piloten zuhoeren" )
	},
	"KanisterAuffuellenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "kanister auffuellen" )
	},
	"TuerenZuIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "tueren zu" )
	},
	"ZumMonsterIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum monster" )
	},
}