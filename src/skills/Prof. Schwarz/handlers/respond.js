'use strict'

var AlexaSkill = require('../AlexaSkill')
var dynamo = require('./dynamoDB')
var utils = require('./utils')

module.exports = {

  readSceneWithCard: function ( scene, session, response ) {

	var alternativText = false
  
	if(scene.playerReceivesItem && session.attributes.items != null) {
		
		if(session.attributes.items.indexOf(scene.itemName) == -1) {
			session.attributes.items.push(scene.itemName)
			console.log( 'Get item ' + scene.itemName )
		} else {
			//skipSceneWithoutCard( scene, session, response )
			alternativText = true
			console.log( 'Already got item' )
			//return;
		}			
	}
	
	if(scene.requiresItem && session.attributes.items != null) {

		if(session.attributes.items.indexOf(scene.itemName) == -1) {
			console.log( 'needs item ' + scene.itemName )
		} else {
			//skipSceneWithoutCard( scene, session, response )
			alternativText = true
			console.log( 'Got needed item' )
			//return;
		}	
		
		/*if(currentScene.options.length > 0) {
			var nextScene = utils.findNextScene( currentScene, currentScene.options[0] );
			session.attributes.breadcrumbs.push( currentScene.id )
			session.attributes.currentSceneId = nextScene.id
			scene = nextScene
			//readSceneWithCard( nextScene, session, response )
			//return;
		}*/
	}

	var json = buildResponse( scene )
    
	if(json.speechOutput.ssml.split('|') != json.speechOutput.ssml) {
		if(alternativText) {
			json.speechOutput.ssml = '<speak>' + json.speechOutput.ssml.split('|')[1]
			json.repromptOutput.ssml = '<speak>' + "Was möchtest du jetzt ?" + '</speak>'
		} else {
			json.speechOutput.ssml = json.speechOutput.ssml.split('|')[0] + scene.voice.prompt.trim() + '</speak>'
		}
	}
	
	dynamo.putUserState( session, function ( data ) {
      console.log( data.message )
      response.askWithCard(
        json.speechOutput,
        json.repromptOutput,
        json.cardTitle,
        json.cardOutput,
        json.cardImage
      )
    })
  },

  exitWithCard: function ( scene, session, response ) {
    var json = buildResponse( scene )
    dynamo.putUserState( session, function ( data ) {
      console.log( data.message )
      response.tellWithCard(
        json.speechOutput,
        json.cardTitle,
        json.cardOutput,
        json.cardImage
      )
    })
  },
  
  skipSceneWithoutCard: function ( scene, session, response ) {
    var json = buildResponse( scene )
    response.ask(
        json.repromptOutput,
        json.repromptOutput
      )
  },
  
  readInventoryWithCard: function ( scene, session, response ) {
   
	var itemOutput = ""
	
	if(session.attributes.items != null && session.attributes.items.length > 0) {
		itemOutput = "Du hast ";
		for(var i = 0; i < session.attributes.items.length; i++) {
			if(session.attributes.items.length > 1 && i == session.attributes.items.length - 1) {
				itemOutput += "und " + session.attributes.items[i] + " "
			} else {
				itemOutput += session.attributes.items[i] + ", "
			}
		}
		itemOutput += "im Rucksack.";
	} else {
		itemOutput = "Dein Rucksack ist leer.";
	}
	itemOutput += " " + scene.voice.prompt.trim();
	var json = buildResponse( scene )
    response.askWithCard(
        itemOutput,
        json.repromptOutput,
        json.cardTitle,
        json.cardOutput,
        json.cardImage
      )
  }

}

function buildResponse ( scene ){

  var voicePrompt = (scene.voice.prompt ? scene.voice.prompt.trim() : "") || buildPrompt( scene, true )
  var cardPrompt  = (scene.card.prompt ? scene.card.prompt.trim() : "")  || buildPrompt( scene, false )

  return {
	  
    // initial text spoken by Alexa
    speechOutput: {
      type: AlexaSkill.SPEECH_OUTPUT_TYPE.SSML,
      ssml: '<speak>' +
            scene.voice.intro.trim() +
            '<break time="200ms"/>' +
            voicePrompt +
            '</speak>'
    },

    // reprompt wird gepielt wenn mehr als 7 Sekunden Stille
    repromptOutput: {
      type: AlexaSkill.SPEECH_OUTPUT_TYPE.SSML,
      ssml: '<speak>' +
            //'Es tut mir Leid.<break time="200ms"/>' +
            voicePrompt +
            '</speak>'
    },

    cardTitle:  scene.card.title || config.skillName,
    cardOutput: scene.card.text.trim() +
              ( scene.card.text.trim() && cardPrompt ? ' ' : '' ) +
                cardPrompt,

    cardImage: scene.card.image || null

  }

}

function buildPrompt ( scene, isForSpeech ) {
  var utils = require('./utils')
  var options = []

  if( !isForSpeech && !scene.generateOptions ) return scene.card.prompt.trim()
  
  if ( scene.voice.prompt ) return scene.voice.prompt.trim()

  var options = scene.options.filter( function ( option ) {
    return ! utils.findResponseBySceneId( option.sceneId ).isHidden
  }).map( function ( option ) {
    return option.utterances[0]
  })

  var hasOptions = ( options.length > 0 )
  if ( ! hasOptions ) return ''

  var preamble = options.length > 1 ? 'Du kannst sagen, ' : 'Sage, '
  return assemble( preamble, options, isForSpeech )
}

function assemble ( preamble, options, isSpeech ) {
  var options = options.map( function ( option, index, array ) {
    if ( array.length > 1 && index === array.length -1 ) {
      return ( isSpeech ? '<break time="100ms" />' : '' ) + 'oder “' + capitalize( option ) + '.”'
    }
    else if ( index == array.length -2 ){
      return '“' + capitalize( option ) + '”'
    }
    else if ( array.length === 1 ) {
      return '“' + capitalize( option ) + '.”'
    }
    else {
      return '“' + capitalize( option ) + ',”'
    }
  })
  return  preamble + options.join(' ')
}

function capitalize ( str ) {
  return str.charAt(0).toUpperCase() + str.slice(1)
}
