var processUtterance = require('./processUtterance')

module.exports = {
	"ResetStateIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "neu anfangen" )
	},
	"RestoreStateIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "fortsetzen" )
	},
	"RepeatOptionsIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "auswahl wiederholen" )
	},
	"RepeatSceneIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nochmal" )
	},
	"GoBackIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zurück" )
	},
	"NextIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "überspringen" )
	},
	"TippIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "tipp" )
	},
	"InventarIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "inventar" )
	},
	"AMAZON.HelpIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "hilfe" )
	},
	"AMAZON.StopIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "stop" )
	},
	"AMAZON.CancelIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "abbrechen" )
	},
	"StartIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "start" )
	},
	"FlurBetretenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "flur betreten" )
	},
	"KomodeIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "komode" )
	},
	"AusgangIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "ausgang" )
	},
	"AbstellraumIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "abstellraum" )
	},
	"TreppeRunterIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "treppe runter" )
	},
	"HaustuerOeffnenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "haustuer oeffnen" )
	},
	"StrasseNachRechtsIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "strasse nach rechts" )
	},
	"UeberDieStrasseIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "ueber die strasse" )
	},
	"StrasseNachLinksIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "strasse nach links" )
	},
	"NachSehenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nach sehen" )
	},
	"JaWeiterIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "ja weiter" )
	},
	"TaxiBenutzenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "taxi benutzen" )
	},
	"AussteigenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "aussteigen" )
	},
	"ZumBahnhofIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum bahnhof" )
	},
	"ZeitungMitnehmenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zeitung mitnehmen" )
	},
	"BahnhofBetretenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "bahnhof betreten" )
	},
	"ZumZugIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum zug" )
	},
	"ZumKioskIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum kiosk" )
	},
	"FahrkartenautomatIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "fahrkartenautomat" )
	},
	"EinsteigenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "einsteigen" )
	},
	"AbteilBetretenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "abteil betreten" )
	},
	"ZugVerlassenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zug verlassen" )
	},
	"BahnhofVerlassenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "bahnhof verlassen" )
	},
	"ZumCampusIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum campus" )
	},
	"ZumGartenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum garten" )
	},
	"GeradeausIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "geradeaus" )
	},
	"HauptgebaeudeIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "hauptgebaeude" )
	},
	"ZumPfoertnerIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum pfoertner" )
	},
	"LaborBetretenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "labor betreten" )
	},
	"BueroBetretenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "buero betreten" )
	},
	"LaborVerlassenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "labor verlassen" )
	},
	"SchranktuerOeffnenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "schranktuer oeffnen" )
	},
	"SchluesselkastenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "schluesselkasten" )
	},
	"ParkplatzIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "parkplatz" )
	},
	"VierEinsAchtAchtFuenfIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "vier eins acht acht fuenf" )
	},
	"BMWIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "b m w" )
	},
	"EnteIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "ente" )
	},
	"AudiIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "audi" )
	},
	"GolfIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "golf" )
	},
	"AbfahrenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "abfahren" )
	},
	"AutoVerlassenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "auto verlassen" )
	},
	"ZumEingangIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum eingang" )
	},
	"HausBetretenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "haus betreten" )
	},
	"ToiletteIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "toilette" )
	},
	"InDenFlurIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "in den flur" )
	},
	"ZumTischIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum tisch" )
	},
	"NeinWillIchNichtIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nein will ich nicht" )
	},
	"AbInsWohnzimmerIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "ab ins wohnzimmer" )
	},
	"KlingelnIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "klingeln" )
	},
	"RegenrinneUntersuchenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "regenrinne untersuchen" )
	},
	"BlumentopfUntersuchenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "blumentopf untersuchen" )
	},
	"FussmatteAnhebenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "fussmatte anheben" )
	},
	"HaustuerAufschliessenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "haustuer aufschliessen" )
	},
	"NachHintenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nach hinten" )
	},
	"AbwartenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "abwarten" )
	},
	"NachDraussenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nach draussen" )
	},
	"LosfahrenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "losfahren" )
	},
	"ZielAIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "ziel a" )
	},
	"ZielBIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "ziel b" )
	},
	"HausAnsehenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "haus ansehen" )
	},
	"AnhaltenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "anhalten" )
	},
	"HinterDasHausIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "hinter das haus" )
	},
	"AufsDachIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "aufs dach" )
	},
	"DurchsFensterIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "durchs fenster" )
	},
	"ZumKofferraumIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum kofferraum" )
	},
	"TuerAufbrechenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "tuer aufbrechen" )
	},
	"AnDerTuerHorchenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "an der tuer horchen" )
	},
	"TuerLeiseOeffnenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "tuer leise oeffnen" )
	},
	"RaumVerlassenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "raum verlassen" )
	},
	"JaWirHelfenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "ja wir helfen" )
	},
	"NeinWirGehenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nein wir gehen" )
	},
	"NeinKeineLustIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nein keine lust" )
	},
	"JaNatuerlichIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "ja natuerlich" )
	},
	"IchVerpruegelIhnIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "ich verpruegel ihn" )
	},
	"IchErpresseIhnIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "ich erpresse ihn" )
	},
	"EineFalleStellenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "eine falle stellen" )
	},
	"VerpruegelnIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "verpruegeln" )
	},
	"ErpressenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "erpressen" )
	},
	"ZumAutoGehenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum auto gehen" )
	},
	"AnhaltenUndAussteigenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "anhalten und aussteigen" )
	},
	"ZurUniIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zur uni" )
	},
	"WirNehmenLinksIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "wir nehmen links" )
	},
	"DieHintertuerBenutzenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "die hintertuer benutzen" )
	},
	"WirGehenRechtsIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "wir gehen rechts" )
	},
	"WirGehenWeiterIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "wir gehen weiter" )
	},
	"FuenfAchtAchtEinsVierIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "fuenf acht acht eins vier" )
	},
	"SecurityAblenkenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "security ablenken" )
	},
	"BuerotuerOeffnenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "buerotuer oeffnen" )
	},
	"EinZeichenGebenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "ein zeichen geben" )
	},
	"ZumVordereingangIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum vordereingang" )
	},
	"ZumBueroVomProfessorIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum buero vom professor" )
	},
	"ZuAdamsGehenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zu adams gehen" )
	},
	"DasNotizbuchZeigenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "das notizbuch zeigen" )
	},
	"FlascheOeffnenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "flasche oeffnen" )
	},
}