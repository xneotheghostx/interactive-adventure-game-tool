var processUtterance = require('./processUtterance')

module.exports = {
	"ResetStateIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "starten" )
	},
	"RestoreStateIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "fortsetzen" )
	},
	"RepeatOptionsIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "auswahl wiederholen" )
	},
	"RepeatSceneIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nochmal" )
	},
	"GoBackIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zurück" )
	},
	"NextIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "überspringen" )
	},
	"AMAZON.HelpIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "hilfe" )
	},
	"AMAZON.StopIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "stop" )
	},
	"AMAZON.CancelIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "abbrechen" )
	},
	"StartIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "start" )
	},
	"AnfangenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "anfangen" )
	},
	"OeffneTuerEinsIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "oeffne tuer eins" )
	},
	"OeffneTuerZweiIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "oeffne tuer zwei" )
	},
	"OeffneTuerDreiIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "oeffne tuer drei" )
	},
	"JaIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "ja" )
	},
	"WeiterIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "weiter" )
	},
	"TreppeRunterIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "treppe runter" )
	},
	"NachRechtsIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nach rechts" )
	},
	"GeradeAusIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "gerade aus" )
	},
	"NachLinksIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nach links" )
	},
	"OeffneDieTuerIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "oeffne die tuer" )
	},
	"TreppeNachObenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "treppe nach oben" )
	},
	"NimmMichMitIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nimm mich mit" )
	},
}