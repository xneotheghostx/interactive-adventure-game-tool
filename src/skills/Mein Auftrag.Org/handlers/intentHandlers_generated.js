var processUtterance = require('./processUtterance')

module.exports = {
	"ResetStateIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "starten" )
	},
	"RestoreStateIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "fortsetzen" )
	},
	"RepeatOptionsIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "auswahl wiederholen" )
	},
	"RepeatSceneIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nochmal" )
	},
	"GoBackIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zurück" )
	},
	"NextIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "überspringen" )
	},
	"TippIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "tipp" )
	},
	"AMAZON.HelpIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "hilfe" )
	},
	"AMAZON.StopIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "stop" )
	},
	"AMAZON.CancelIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "abbrechen" )
	},
	"StartIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "start" )
	},
	"AufNachLondonIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "auf nach london" )
	},
	"AnklopfenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "anklopfen" )
	},
	"VillaVerlassenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "villa verlassen" )
	},
	"EinsteigenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "einsteigen" )
	},
	"NachLiverpoolIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nach liverpool" )
	},
	"HausBetretenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "haus betreten" )
	},
	"AussteigenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "aussteigen" )
	},
	"NachbarinFragenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nachbarin fragen" )
	},
	"NoahsWohnungIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "noahs wohnung" )
	},
	"ZumEingangIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum eingang" )
	},
	"KlingelnIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "klingeln" )
	},
	"NachLinksIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nach links" )
	},
	"TreppeRunterIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "treppe runter" )
	},
	"NachRechtsIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nach rechts" )
	},
	"WeiterGehenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "weiter gehen" )
	},
	"TreppeRaufIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "treppe rauf" )
	},
	"LukeAufstossenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "luke aufstossen" )
	},
	"HotelBetretenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "hotel betreten" )
	},
	"NachSheffieldIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nach sheffield" )
	},
	"InsRestaurantIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "ins restaurant" )
	},
	"WohnungVerlassenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "wohnung verlassen" )
	},
	"ZumBuecherregalIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zum buecherregal" )
	},
	"WoIstWilliamThomsonIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "wo ist william thomson" )
	},
	"HausVerlassenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "haus verlassen" )
	},
	"WasIstMitNoahPassiertIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "was ist mit noah passiert" )
	},
	"WerWarDasAnDerTuerIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "wer war das an der tuer" )
	},
	"NachBlackpoolIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nach blackpool" )
	},
	"NeinIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "nein" )
	},
	"JaIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "ja" )
	},
	"TuerAufmachenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "tuer aufmachen" )
	},
	"FahrstuhlBetretenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "fahrstuhl betreten" )
	},
	"ZimmerBetretenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zimmer betreten" )
	},
	"ZimmerVerlassenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zimmer verlassen" )
	},
	"HotelLobbyIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "hotel lobby" )
	},
	"HierBleibenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "hier bleiben" )
	},
	"HotelVerlassenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "hotel verlassen" )
	},
	"IchBleibeHierIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "ich bleibe hier" )
	},
	"LinksIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "links" )
	},
	"RechtsIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "rechts" )
	},
	"FlurFolgenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "flur folgen" )
	},
	"SchluesselBenutzenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "schluessel benutzen" )
	},
	"KuehlschrankOeffnenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "kuehlschrank oeffnen" )
	},
	"TuerOeffnenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "tuer oeffnen" )
	},
	"PasswortIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "passwort" )
	},
	"WeiterLesenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "weiter lesen" )
	},
	"ZuWilliamGehenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zu william gehen" )
	},
	"ZuNoahGehenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "zu noah gehen" )
	},
	"FrauThomsonAnrufenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "frau thomson anrufen" )
	},
	"RaumVerlassenIntent": function ( intent, session, request, response ) {
		processUtterance( intent, session, request, response, "raum verlassen" )
	},
}