'use strict'

var AlexaSkill = require('../AlexaSkill')
var dynamo = require('./dynamoDB')

module.exports = {

  readSceneWithCard: function ( scene, session, response ) {
    var json = buildResponse( scene )
    dynamo.putUserState( session, function ( data ) {
      console.log( data.message )
      response.askWithCard(
        json.speechOutput,
        json.repromptOutput,
        json.cardTitle,
        json.cardOutput,
        json.cardImage
      )
    })
  },

  exitWithCard: function ( scene, session, response ) {
    var json = buildResponse( scene )
    dynamo.putUserState( session, function ( data ) {
      console.log( data.message )
      response.tellWithCard(
        json.speechOutput,
        json.cardTitle,
        json.cardOutput,
        json.cardImage
      )
    })
  },
  
  skipSceneWithoutCard: function ( scene, session, response ) {
    var json = buildResponse( scene )
    response.ask(
        json.repromptOutput,
        json.repromptOutput
      )
  }

}

function buildResponse ( scene ){

  var voicePrompt = (scene.voice.prompt ? scene.voice.prompt.trim() : "") || buildPrompt( scene, true )
  var cardPrompt  = (scene.card.prompt ? scene.card.prompt.trim() : "")  || buildPrompt( scene, false )

  return {

    // initial text spoken by Alexa
    speechOutput: {
      type: AlexaSkill.SPEECH_OUTPUT_TYPE.SSML,
      ssml: '<speak>' +
            scene.voice.intro.trim() +
            '<break time="200ms"/>' +
            voicePrompt +
            '</speak>'
    },

    // reprompt wird gepielt wenn mehr als 7 Sekunden Stille
    repromptOutput: {
      type: AlexaSkill.SPEECH_OUTPUT_TYPE.SSML,
      ssml: '<speak>' +
            //'Es tut mir Leid.<break time="200ms"/>' +
            voicePrompt +
            '</speak>'
    },

    cardTitle:  scene.card.title || config.skillName,
    cardOutput: scene.card.text.trim() +
              ( scene.card.text.trim() && cardPrompt ? ' ' : '' ) +
                cardPrompt,

    cardImage: scene.card.image || null

  }

}

function buildPrompt ( scene, isForSpeech ) {
  var utils = require('./utils')
  var options = []

  if( !isForSpeech && !scene.generateOptions ) return scene.card.prompt.trim()
  
  if ( scene.voice.prompt ) return scene.voice.prompt.trim()

  var options = scene.options.filter( function ( option ) {
    return ! utils.findResponseBySceneId( option.sceneId ).isHidden
  }).map( function ( option ) {
    return option.utterances[0]
  })

  var hasOptions = ( options.length > 0 )
  if ( ! hasOptions ) return ''

  var preamble = options.length > 1 ? 'Du kannst sagen, ' : 'Sage, '
  return assemble( preamble, options, isForSpeech )
}

function assemble ( preamble, options, isSpeech ) {
  var options = options.map( function ( option, index, array ) {
    if ( array.length > 1 && index === array.length -1 ) {
      return ( isSpeech ? '<break time="100ms" />' : '' ) + 'oder “' + capitalize( option ) + '.”'
    }
    else if ( index == array.length -2 ){
      return '“' + capitalize( option ) + '”'
    }
    else if ( array.length === 1 ) {
      return '“' + capitalize( option ) + '.”'
    }
    else {
      return '“' + capitalize( option ) + ',”'
    }
  })
  return  preamble + options.join(' ')
}

function capitalize ( str ) {
  return str.charAt(0).toUpperCase() + str.slice(1)
}
