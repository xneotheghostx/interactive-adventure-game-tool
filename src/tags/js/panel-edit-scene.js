(function(tagger) {
  if (typeof define === 'function' && define.amd) {
    define(['riot'], function(riot) { tagger(riot); });
  } else if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    tagger(require('riot'));
  } else {
    tagger(window.riot);
  }
})(function(riot) {
riot.tag2('panel-edit-scene', '<h2> <strong>{title}</strong> <span if="{title && subtitle}">-</span> <em>{subtitle}</em> </h2> <collapsible title="Erweitert" id="$collapsibleSceneAdvanced"> <div class="input-row"> <label for="$requiresItem"> Item benötigt </label> <div class="radio-set"> <span class="radio-button"> <input type="radio" value="true" name="$requiresItem" id="$requiresItemTrue" onchange="{parent.save}" __checked="{parent.scene.requiresItem === true}"> <label for="$requiresItemTrue"> Ja </label> </span> <span class="radio-button"> <input type="radio" value="false" name="$requiresItem" id="$requiresItemFalse" onchange="{parent.save}" __checked="{parent.scene.requiresItem === false}"> <label for="$requiresItemFalse"> Nein </label> </span> </div> </div> <div class="input-row"> <label for="$playerReceivesItem"> Spieler erhält Item </label> <div class="radio-set"> <span class="radio-button"> <input type="radio" value="true" name="$playerReceivesItem" id="$playerReceivesItemTrue" onchange="{parent.save}" __checked="{parent.scene.playerReceivesItem === true}"> <label for="$playerReceivesItemTrue"> Ja </label> </span> <span class="radio-button"> <input type="radio" value="false" name="$playerReceivesItem" id="$playerReceivesItemFalse" onchange="{parent.save}" __checked="{parent.scene.playerReceivesItem === false}"> <label for="$playerReceivesItemFalse"> Nein </label> </span> </div> </div> <div class="input-row"> <label for="$itemName"> Item Name </label> <autogrow-textarea id="$itemName" onblur="{parent.save}" value="{parent.scene.itemName}"></autogrow-textarea> </div> <div class="input-row"> <label for="$inputIsHidden"> Unsichtbar (Geheim) </label> <div class="radio-set"> <span class="radio-button"> <input type="radio" value="true" name="$inputIsHidden" id="$inputIsHiddenTrue" onchange="{parent.save}" __checked="{parent.scene.isHidden === true}"> <label for="$inputIsHiddenTrue"> Ja </label> </span> <span class="radio-button"> <input type="radio" value="false" name="$inputIsHidden" id="$inputIsHiddenFalse" onchange="{parent.save}" __checked="{parent.scene.isHidden === false}"> <label for="$inputIsHiddenFalse"> Nein </label> </span> </div> </div> <div class="input-row" if="{parent.scene.voice.prompt.trim() === \'\'}"> <label for="$inputReadPreviousOptions"> Wahlmöglichkeit der vorherigen Szene </label> <div class="radio-set"> <span class="radio-button"> <input type="radio" value="true" name="$inputReadPreviousOptions" id="$inputReadPreviousOptionsTrue" onchange="{parent.save}" __checked="{parent.scene.readPreviousOptions === true}"> <label for="$inputReadPreviousOptionsTrue"> Ja </label> </span> <span class="radio-button"> <input type="radio" value="false" name="$inputReadPreviousOptions" id="$inputReadPreviousOptionsFalse" onchange="{parent.save}" __checked="{parent.scene.readPreviousOptions === false}"> <label for="$inputReadPreviousOptionsFalse"> Nein </label> </span> </div> </div> <div class="input-row"> <label for="$inputColor"> Farbe </label> <div class="radio-set"> <span class="radio-button"> <input type="radio" value="default" name="$inputColor" id="$inputColorDefault" onchange="{parent.save}" __checked="{parent.scene.color === \'default\'}"> <label for="$inputColorDefault"> <span class="color-swatch default"></span> </label> </span> <span class="radio-button"> <input type="radio" value="green" name="$inputColor" id="$inputColorGreen" onchange="{parent.save}" __checked="{parent.scene.color === \'green\'}"> <label for="$inputColorGreen"> <span class="color-swatch green"></span> </label> </span> <span class="radio-button"> <input type="radio" value="red" name="$inputColor" id="$inputColorRed" onchange="{parent.save}" __checked="{parent.scene.color === \'red\'}"> <label for="$inputColorRed"> <span class="color-swatch red"></span> </label> </span> <span class="radio-button"> <input type="radio" value="yellow" name="$inputColor" id="$inputColorYellow" onchange="{parent.save}" __checked="{parent.scene.color === \'yellow\'}"> <label for="$inputColorYellow"> <span class="color-swatch yellow"></span> </label> </span> </div> </div> </collapsible> <div class="tabs"> <div class="tab"> <input type="radio" value="card" name="$inputTab" id="$inputTabCard" onchange="{onChangeTab}" __checked="{currentTab === \'card\'}"> <label for="$inputTabCard"> Karte </label> </div> <div class="tab"> <input type="radio" value="voice" name="$inputTab" id="$inputTabVoice" onchange="{onChangeTab}" __checked="{currentTab === \'voice\'}"> <label for="$inputTabVoice"> Stimme </label> </div> <div class="tab"> <input type="radio" value="session" name="$inputTab" id="$inputTabSession" onchange="{onChangeTab}" __checked="{currentTab === \'session\'}"> <label for="$inputTabSession"> Session </label> </div> </div> <div class="tab-content" show="{currentTab === \'card\'}"> <div class="input-row"> <label for="$title"> Name </label> <autogrow-textarea id="$title" onblur="{save}" value="{scene.card.title}"></autogrow-textarea> </div> <div class="input-row"> <label for="$text"> Text </label> <autogrow-textarea id="$text" onblur="{save}" value="{scene.card.text}"></autogrow-textarea> </div> <div class="input-row"> <label for="$cardPrompt"> Standardeinstellung aufheben <small>(Optional)</small> </label> <autogrow-textarea id="$cardPrompt" onblur="{save}" value="{scene.card.prompt}"></autogrow-textarea> </div> <collapsible title="Bilder" id="$collapsibleCardImage"> <img if="{parent.scene.card.image.smallImageUrl}" riot-src="{parent.scene.card.image.smallImageUrl}" alt="Small Card Image"> <div class="input-row"> <label for="$inputSmallImageUrl"> Bild Klein URL <small>(108&times;108)</small> </label> <input type="url" id="$inputSmallImageUrl" onblur="{parent.save}" value="{parent.scene.card.image.smallImageUrl}"> </div> <img if="{parent.scene.card.image.largeImageUrl}" riot-src="{parent.scene.card.image.largeImageUrl}" alt="Large Card Image"> <div class="input-row"> <label for="$inputLargeImageUrl"> Bild Groß URL <small>(512&times;512)</small> </label> <input type="url" id="$inputLargeImageUrl" onblur="{parent.save}" value="{parent.scene.card.image.largeImageUrl}"> </div> <br> <a class="hotspot float-right" onmouseenter="{parent.toggleNotice}" onmouseleave="{parent.toggleNotice}" data-target="#imageNotice"> <i class="fa fa-info fa-lg"></i> </a> <div id="imageNotice" class="notice text-center display-none"> <p> <small> <strong>Hinweis</strong>: Alle Bilder URLs müssen (HTTPS) sein </small> </p> </div> </collapsible> <br> <a class="hotspot float-right" onmouseenter="{toggleNotice}" onmouseleave="{toggleNotice}" data-target="#cardNotice"> <i class="fa fa-info fa-lg"></i> </a> <div id="cardNotice" class="notice display-none"> <p> <strong>Standardeinstellung aufheben</strong>:<br> Standardmäßig wird Alexa eine visuelle Karte zur Verfügung stellen (Alexa App) in der alle Antwortmöglichkeiten in Textform für den Benutzer stehen. </p> <p> Wenn du diese Standardverhalten überschreiben möchtest, Gebe hier deine Eingabeaufforderung ein. </p> <p> <small> <strong>Hinweis:</strong> Optionen, zu "versteckten" Szenen werden ignoriert. </small> </p> </div> </div> <div class="tab-content" show="{currentTab === \'voice\'}"> <div class="input-row"> <label for="$intro"> Sprache </label> <autogrow-textarea id="$intro" onblur="{save}" value="{scene.voice.intro}"></autogrow-textarea> </div> <div class="input-row"> <label for="$voicePrompt"> Standardeinstellung aufheben <small>(Optional)</small> </label> <autogrow-textarea id="$voicePrompt" onblur="{save}" value="{scene.voice.prompt}"></autogrow-textarea> </div> <br> <a class="hotspot float-right" onmouseenter="{toggleNotice}" onmouseleave="{toggleNotice}" data-target="#ssmlNotice"> <i class="fa fa-info fa-lg"></i> </a> <div id="ssmlNotice" class="notice display-none"> <p> <strong>Standardeinstellung aufheben</strong>:<br> Standardmäßig wird Alexa den Benutzer auffordern, auf alles zu antworten. Wobei alle derzeit verfügbaren Optionen möglich sind. </p> <p> Wenn du dieses Standardverhalten ändern möchtest, gebe hier deinen Sprachtext mit Antwort ein. <br> <small> <strong>Hinweis:</strong> Unter Erweitert : Die Option <strong>verstecken</strong> wählen, dann wird die Szene ignoriert. </small> </p> <p> <strong>Unterstützte SSML Tags</strong> </p> <code class="row"> <ul class="list-unstyled"> <li>&lt;audio&gt;</li> <li>&lt;break&gt;</li> <li>&lt;speak&gt;</li> <li>&lt;say-as&gt;</li> </ul> <ul class="list-unstyled"> <li>&lt;phoneme&gt;</li> <li>&lt;s&gt;</li> <li>&lt;p&gt;</li> <li>&lt;w&gt;</li> </ul> </code> <p> Siehe <a href="https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/speech-synthesis-markup-language-ssml-reference" target="_blank">Alexa <code>SSML</code> documentation</a> <br> <small> <strong>Hinweis</strong>: Alle URLs müssen (HTTPS) sein </small> </p> </div> </div> <div class="tab-content" show="{currentTab === \'session\'}"> <div class="input-row"> <select id="$variableMode" onblur="{save}" style="font-size: .8em; color: #ddd; font-weight: 100; font-family: Helvetica,\'Helvetica Nueue\',arial; border: none;"> <option style="background-color: #333" value="replace" __selected="{scene.session.variableMode === \'replace\'}">Variable ersetzen</option> <option style="background-color: #333" value="add" __selected="{scene.session.variableMode === \'add\'}">Variable addieren</option> </select> <autogrow-textarea style="display: inline-block;" id="$variableSet" onblur="{save}" placeholder="Name" value="{scene.session.variableSet}"></autogrow-textarea> <autogrow-textarea style="display: inline-block;" id="$variableSetValue" onblur="{save}" placeholder="Wert" value="{scene.session.variableSetValue}"></autogrow-textarea> </div> <div class="input-row"> <label for="$variableGet"> Variable abgleichen </label> <autogrow-textarea style="display: inline-block;" id="$variableGet" onblur="{save}" placeholder="Name" value="{scene.session.variableGet}"></autogrow-textarea> <autogrow-textarea style="display: inline-block;" id="$variableGetValue" onblur="{save}" placeholder="Wert" value="{scene.session.variableGetValue}"></autogrow-textarea> </div> <div class="input-row"> <label for="$inputSessionAction"> Aktion, wenn Feld oben leer oder Variable mit Wert übereinstimmt </label> <div class="radio-set"> <span class="radio-button"> <input type="radio" value="default" name="$inputSessionAction" id="$inputDefault" onchange="{save}" __checked="{scene.session.action === \'default\'}"> <label for="$inputDefault"> <span>Nichts</span> </label> </span> <span class="radio-button"> <input type="radio" value="teleport" name="$inputSessionAction" id="$inputTeleport" onchange="{save}" __checked="{scene.session.action === \'teleport\'}"> <label for="$inputTeleport"> <span>Teleport</span> </label> </span> <span class="radio-button"> <input type="radio" value="alternative" name="$inputSessionAction" id="$inputAlternative" onchange="{save}" __checked="{scene.session.action === \'alternative\'}"> <label for="$inputAlternative"> <span>Alternativer Text</span> </label> </span> </div> </div> <div class="input-row"> <autogrow-textarea id="$sessionActionValue" placeholder="Szenen-ID oder Text" onblur="{save}" value="{scene.session.actionValue}"></autogrow-textarea> </div> </br> <a class="hotspot float-right" onmouseenter="{toggleNotice}" onmouseleave="{toggleNotice}" data-target="#sessionNotice"> <i class="fa fa-info fa-lg"></i> </a> <div id="sessionNotice" class="notice display-none"> <p> <strong>Session-Attribute</strong>:<br> <li> Bei Gegenständen die sichbar im Inventar sein sollen, <p>wird ein <kbd>Item_</kbd> vor dem Gegenstandsnamen gesetzt.</p> </li> </p> </div> </div>', '', '', function(opts) {
var _this = this;

this.title = null;
this.scene = null;
this.currentTab = null;

this.mixin('toggleNotice');

var subRoute = riot.route.create();

subRoute('/', function (x) {
  _this.scene = null;
  _this.update();
});

subRoute('/response:*/*', function (utterance, tab) {
  _this.title = 'Response';
  _this.subtitle = utterance;
  _this.scene = opts.config.responses[utterance];
  _this.currentTab = tab === 'card' ? 'card' : 'voice';
  _this.update();
});

subRoute('/scene:START/*', function (tab) {
  _this.title = 'Start';
  _this.subtitle = 'Skill Start';
  _this.scene = opts.scenes[0];
  _this.currentTab = tab;
  _this.update();
});

subRoute('/scene:*/*', function (sceneId, tab) {
  _this.scene = opts.scenes.find(function (scene) {
    return scene.id === Number(sceneId);
  });

  if (!_this.scene) return riot.route('/');
  _this.title = 'Scene ' + _this.scene.id;
  _this.subtitle = _this.scene.card.title;
  _this.currentTab = tab;
  _this.update();
});

this.onChangeTab = function (e) {
  var hash = location.hash.split('/');
  hash = hash.slice(1, hash.length - 1);
  hash.push(e.target.value);
  riot.route(hash.join('/'));
};

this.save = function (e) {
  var $advanced = _this.$collapsibleSceneAdvanced._tag;
  var $colorSelection = [$advanced.$inputColorDefault, $advanced.$inputColorGreen, $advanced.$inputColorRed, $advanced.$inputColorYellow].find(function (x) {
    return x.checked;
  });

  var $actionSelection = [_this.$inputDefault, _this.$inputTeleport, _this.$inputAlternative].find(function (x) {
    return x.checked;
  });

  _this.scene.color = $colorSelection ? $colorSelection.value : 'default';
  _this.scene.isHidden = $advanced.$inputIsHiddenTrue.checked;
  _this.scene.requiresItem = $advanced.$requiresItemTrue.checked;
  _this.scene.playerReceivesItem = $advanced.$playerReceivesItemTrue.checked;
  _this.scene.readPreviousOptions = $advanced.$inputReadPreviousOptionsTrue.checked;
  _this.scene.itemName = $advanced.$itemName.value.trim();

  _this.scene.voice = {
    intro: _this.$intro.value.trim(),
    prompt: _this.$voicePrompt.value.trim()
  };

  var $cardImage = _this.$collapsibleCardImage._tag;
  _this.scene.card = {
    title: _this.$title.value.trim(),
    text: _this.$text.value.trim(),
    prompt: _this.$cardPrompt.value.trim(),
    image: {
      largeImageUrl: $cardImage.$inputLargeImageUrl.value.trim() || null,
      smallImageUrl: $cardImage.$inputSmallImageUrl.value.trim() || null
    }
  };

  _this.scene.session = {
    variableMode: _this.$variableMode.value,
    variableGet: _this.$variableGet.value.trim(),
    variableGetValue: _this.$variableGetValue.value.trim(),
    variableSet: _this.$variableSet.value.trim(),
    variableSetValue: _this.$variableSetValue.value.trim(),
    action: $actionSelection ? $actionSelection.value : 'default',
    actionValue: _this.$sessionActionValue.value.trim()
  };

  _this.subtitle = _this.scene.card.title;

  riot.update();
};
});


});