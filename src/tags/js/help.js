(function(tagger) {
  if (typeof define === 'function' && define.amd) {
    define(['riot'], function(riot) { tagger(riot); });
  } else if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    tagger(require('riot'));
  } else {
    tagger(window.riot);
  }
})(function(riot) {
riot.tag2('help', '<help-modal onclick="{stopPropagation}"> <help-modal-title> Hilfe <button type="button" name="close" onclick="{close}"> &times; </button> </help-modal-title> <help-modal-content> <collapsible title="Allgemeine Sprachbefehle"> <p>Legen Sie die Äußerungen fest, die die verfügbaren Sprachbefehle (Trigger) auslösen, wie z.B. &quot;<em>zurück</em>&quot; oder &quot;<em>Wiederholungsoptionen</em>&quot;. Jeder Befehl unterstützt mehrere Äußerungen und jede Äußerung muss durch eine neue Zeile getrennt sein.</p> </collapsible> <collapsible title="Allgemeine Antworten"> <p>Hier sind Links zu Alexa\'s Antworten auf <strong>Allgemeine Befehle</strong> wie:</p> <ul> <li> <p><strong>Wiederherstellungs aufforderung</strong>: Hiermit reagiert Alexa beim Starten des Skills, wenn ein vorheriger Spielstand in der Dynamo DB-Tabelle gefunden wurde (Die im Abschnitt AWS-Einstellungen unten definiert ist).</p> </li> <li> <p><strong>Unbekannt</strong>: Hiermit gibt Alexa eine Antwort, wenn ein Befehl nicht verstanden oder zugewiesen werden kann.</p> </li> <li> <p><strong>Hilfe</strong>: Hiermit Antwortet Alexa, wenn der Benutzer um Hilfe bittet.</p> </li> <li> <p><strong>Ende</strong>: Hiermit Antwortet Alexa, wenn der Benutzer das Spiel beenden oder abbrechen will.</p> </li> </ul> </collapsible> <collapsible title="AWS Einstellungen"> <p>Diese Optionen helfen verschiedene AWS-Technologien die von den Skill genutzt werden, zu konfigurieren. Der Parameter <strong>AWS Lambda Function Name</strong> ist notwendig, sonst können Sie Ihre Änderungen nicht auf die AWS Lambda-Plattform hochladen.</p> <ul> <li> <p><strong>AWS Lambda Function Name</strong>: Dies muss mit dem Namen übereinstimmen, den Sie beim Erstellen Ihrer Lambda-Funktion über die AWS-Administrationsschnittstelle angegeben haben.</p> </li> <li> <p><strong>Dynamo DB Table Name</strong>: <em>(Diese Feld ist optional)</em> Dieser Name wird verwendet, um die Dynamo DB-Tabelle zu identifizieren, auf der der aktuelle Spielstand gespeichert wird. Wird kein Dynamo DB-Tabellenname verwendet, kann für spätere Wiederaufnahme des Spiels auch kein Spielstand genutzt werden. Die angegebene Tabelle muss auf dasselbe AWS-Konto zugreifen, das nachstehend angegeben ist.</p> </li> <li> <p><strong>AWS Profile</strong>: <em>(Dieses Feld ist optional)</em> Um deinen Code in die AWS-Lambda-Console hochzuladen, die in der Cloud gehostet wird, müssen die mit deinem AWS-Konto verknüpften Anmeldeinformationen mit den erforderlichen IAM-Berechtigungen übereinstimmen. AWS Anmeldeinformationen sollten hier gespeichert werden @ ~/.aws/credentials. Siehe <a href="http://docs.aws.amazon.com/AWSJavaScriptSDK/guide/node-configuring.html#Setting_AWS_Credentials">Einstellungen AWS Anmeldeinformationen</a> für mehr Details.</p> </li> </ul> </collapsible> <collapsible title="Rucksack / Inventar - Einstellungen"> <p></p> <p><strong>Item Erhalten</strong> hier gilt vollgenes :</p> <ul> <li> <p><strong>1. Text</strong> Hier steht der Text für das erhaltene Item. Zum Beispiel: <strong>Schlüssel gefunden.</strong>. Getrennt von einem <kbd>|</kbd> kommt der 2. Text.</p> </li> <li> <p><strong>2. Text</strong> Hier steht der Text wenn der Gegenstand bereits im Invenar ist. Zum Beispiel: <strong>Der Raum ist leer</strong>.</p> </li> </ul> <p><strong>Item Benötigt</strong> hier gilt vollgenes :</p> <ul> <li> <p><strong>1. Text</strong> Hier steht der Text wenn das Item fehlt. Zum Beispiel: <strong>Der Tür geht nicht auf.</strong>. Getrennt von einem <kbd>|</kbd> kommt der 2. Text.</p> </li> <li> <p><strong>2. Text</strong> Hier steht der Text für den benutzten Item. Zum Beispiel: <strong>Klasse der Schlüssel passte.</strong></p> </li> </ul> </collapsible> <collapsible title="Tastaturkürzel"> <dl> <div> <dt>Speichern</dt> <dd> <kbd>⌘</kbd> + <kbd>S</kbd> </dd> </div> <div> <dt>Hochladen</dt> <dd> <kbd>⌘</kbd> + <kbd>U</kbd> </dd> </div> <div> <dt>Hilfe</dt> <dd> <kbd>⌘</kbd> + <kbd>H</kbd> </dd> </div> </dl> </collapsible> </help-modal-content> </help-modal>', '', 'if="{isVisible}" onclick="{close}"', function(opts) {
var _this = this;

this.isVisible = false;

var subRoute = riot.route.create();

subRoute('/help', function (x) {
  _this.isVisible = true;
  _this.update();
});

subRoute('/..', function (x) {
  _this.isVisible = false;
  _this.update();
});

this.stopPropagation = function (e) {
  e.stopPropagation();
};

this.close = function (e) {
  e.stopPropagation();
  history.back();
};
});


});