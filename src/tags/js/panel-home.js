(function(tagger) {
  if (typeof define === 'function' && define.amd) {
    define(['riot'], function(riot) { tagger(riot); });
  } else if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    tagger(require('riot'));
  } else {
    tagger(window.riot);
  }
})(function(riot) {
riot.tag2('panel-home', '<h2> Home </h2> <select id="skillSelect"> <option value="" disabled selected style="display:none;">Skill auswählen:</option> <option each="{skills}" value="{name}"> {name} </option> </select> <collapsible title="Allgemeine Sprachbefehle "> <div class="input-row" each="{parent.commands}"> <label>{title}</label> <autogrow-textarea id="$command-{parent.command}" onblur="{parent.parent.saveCommand}" value="{utterances.slice().join(\'\\n\')}"></autogrow-textarea> </div> <br> <a class="hotspot float-right" onmouseenter="{parent.toggleNotice}" onmouseleave="{parent.toggleNotice}" data-target="#globalCommandNotice"> <i class="fa fa-info fa-lg"></i> </a> <div id="globalCommandNotice" class="notice display-none"> <p> <strong>Allgemeine Sprachbefehle </strong>:<br> Diese Befehle sind Allgemein innerhalb Ihrer Alexa Skill zugänglich. Sie können jederzeit im Kontext durch das Skill aufgerufen werden. </p> <p> Jeder Befehl unterstützt mehrere Äußerungen. Jede Äußerung muss durch eine neue Zeile getrennt werden. </p> </div> </collapsible> <collapsible title="Allgemeine Antworten "> <ul class="list-unstyled"> <li each="{parent.responses}"> <a href="#/response:{key}/card">{title}</a> </li> </ul> <br> <a class="hotspot float-right" onmouseenter="{parent.toggleNotice}" onmouseleave="{parent.toggleNotice}" data-target="#globalResponseNotice"> <i class="fa fa-info fa-lg"></i> </a> <div id="globalResponseNotice" class="notice display-none"> <p> <strong>Allgemeine Antworten</strong>:<br> Konfigurieren Sie die Antworten für die verschiedenen <em>Allgemeine Antworten</em>. </p> </div> </collapsible> <collapsible title="AWS Einstellungen" id="$collapsibleGlobalSettings"> <div class="input-row"> <label for="$inputLambdaName"> AWS Lambda Function Name </label> <input type="text" id="$inputLambdaName" data-setting="lambdaName" onblur="{parent.updateGlobalSetting}" value="{parent.opts.config.lambdaName}"> </div> <div class="input-row"> <label for="$inputDynamoTableName"> Dynamo DB Tabellen Name <small>(Optional)</small> </label> <input type="text" id="$inputDynamoTableName" data-setting="dynamoTableName" onblur="{parent.updateGlobalSetting}" value="{parent.opts.config.dynamoTableName}"> </div> <div class="input-row"> <label for="$inputawsProfileName"> AWS Profile <small>(Optional)</small> </label> <input type="text" id="$inputawsProfileName" data-setting="awsProfileName" onblur="{parent.updateGlobalSetting}" value="{parent.opts.config.awsProfileName}"> </div> <br> <a class="hotspot float-right" onmouseenter="{parent.toggleNotice}" onmouseleave="{parent.toggleNotice}" data-target="#globalAwsNotice"> <i class="fa fa-info fa-lg"></i> </a> <div id="globalAwsNotice" class="notice display-none"> <p> <strong>Dynamo DB-Tabellenname</strong> <br> Wenn kein Dynamo DB-Tabellenname angegeben wird, wird der Benutzerstatus nicht in der Datenbank Gespeichert. </p> <p> <strong>AWS Profile</strong> <br> AWS Anmeldeinformationen sollten hier gespeichert werden @ <code>~/.aws/credentials.</code><br> Siehe <a href="http://docs.aws.amazon.com/AWSJavaScriptSDK/guide/node-configuring.html#Setting_AWS_Credentials" target="_blank">Einstellungen AWS Anmeldeinformationen</a> für mehr Details. </p> </div> </collapsible>', '#skillSelect option {background-color: #333;}', '', function(opts) {
var _this = this;

this.skills = Object.keys(opts.skillnames).map(function (key) {
		return {
				name: opts.skillnames[key]
		};
});

this.commands = [];
this.responses = [];

this.mixin('toggleNotice');

function camelCaseToWords(str) {
		return str.match(/^[a-z]+|[A-Z][a-z]*/g).map(function (x) {
				return x[0].toUpperCase() + x.substr(1).toLowerCase();
		}).join(' ');
};

this.on('mount', function (x) {
		console.log(opts.skillnames);
		_this.responses = Object.keys(opts.config.responses).map(function (key) {
				return {
						key: key,
						title: key.charAt(0).toUpperCase() + key.substr(1)
				};
		});
		_this.commands = Object.keys(opts.config.commands).map(function (command) {
				return {
						command: command,
						utterances: opts.config.commands[command],
						title: camelCaseToWords(command.replace('AMAZON.', '').replace('Intent', ''))
				};
		});

		document.getElementById("skillSelect").addEventListener("change", skillChange);

		var url = new URL(window.location.href).searchParams.get("skill");
		if (url != null) document.getElementById("skillSelect").value = url;
});

this.updateGlobalSetting = function (e) {
		var name = e.currentTarget.dataset.setting;
		_this[name] = opts.config[name] = e.currentTarget.value.trim();
};

this.saveCommand = function (e) {
		var value = e.target.value;
		opts.config.commands[e.item.command] = value.trim().split('\n').filter(function (x) {
				return x !== '';
		});
};

function skillChange() {
		setGetParameter("skill", document.getElementById("skillSelect").value);
}

function setGetParameter(paramName, paramValue) {
		var url = window.location.href;
		var hash = location.hash;
		url = url.replace(hash, '');
		if (url.indexOf(paramName + "=") >= 0) {
				var prefix = url.substring(0, url.indexOf(paramName));
				var suffix = url.substring(url.indexOf(paramName));
				suffix = suffix.substring(suffix.indexOf("=") + 1);
				suffix = suffix.indexOf("&") >= 0 ? suffix.substring(suffix.indexOf("&")) : "";
				url = prefix + paramName + "=" + paramValue + suffix;
		} else {
				if (url.indexOf("?") < 0) url += "?" + paramName + "=" + paramValue;else url += "&" + paramName + "=" + paramValue;
		}
		window.location.href = url + hash;
}
});


});