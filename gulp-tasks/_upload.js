var fs = require('fs')
var path = require('path')
var gulp = require('gulp')
var package = require('../package.json')
var $ = require('gulp-load-plugins')()

var skillName = "skill"

gulp.task('upload', ['generate'], function ( cb ) {

  var skillName = fs.readFileSync('./src/skills/currentSkill.json','utf8')

  var config = JSON.parse(fs.readFileSync('./src/skills/' + skillName + '/models/config.json','utf8'))

  var opts = {
    profile: config.awsProfileName
  }

  return gulp.src('./src/skills/' + skillName + '/**/*.{js,json}', { base: "./src/skills/" + skillName })
    .pipe(
      $.rename( function ( path ) {
        path.dirname = path.dirname.replace('src/skills/' + skillName, '' )
      })
    )
    .pipe( $.zip('dist.zip') )
    .pipe( $.awslambda( config.lambdaName, opts ) )
    .pipe( gulp.dest('./src/skills/' + skillName) )

})
